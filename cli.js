#!/usr/bin/env node
'use strict';
const readline = require('readline');
const calls = require ("./functions.js")

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var recursiveAsyncReadLine = function () {
console.log("Suas opções: \n\n Criar conta bancária \n(create-account-bank --cpf [cpf] --type [savings, current]) \n\n Ver Saldo \n(show-balance --cpf [cpf]) \n\n Deposito \n(deposit --cpf [cpf] --value [value])\n\n Saque \n(withdraw --cpf [cpf] --value [value]) ")
rl.question('\nO que você quer fazer hoje? ', (answer) => {

	answer = answer.split(" ")


	switch (answer[0]) {
		case "create-account-bank":
			const accountBank = calls.createAccountBank(answer[2], answer[4])
			console.log(accountBank)
			
			recursiveAsyncReadLine();
			break;
		case "show-balance":
			const showBalance = calls.showBalance(answer[2])
			console.log(showBalance)
			
			recursiveAsyncReadLine();
			break;
		case "deposit":
			const deposit = calls.deposit(answer[2], answer[4])
			console.log(deposit)
			
			recursiveAsyncReadLine();
			break;
		case "withdraw":
			const withDraw = calls.withDraw(answer[2], answer[4])
			console.log(withDraw)
			
			recursiveAsyncReadLine();
			break;
		case "exit":
			return rl.close()
		default:
			console.log('Opção não encontrada')
	}
  });
  
}

recursiveAsyncReadLine();

