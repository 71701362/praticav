var bankAccounts = [{}];

exports.createAccountBank = (cpf, type) => {
    
    bankAccounts.map((bankAccount) => {
        if ((bankAccount.cpf) == cpf) {
            throw 'Conta já existente'
        }
    })

    bankAccounts = bankAccounts.concat({type: type, cpf: cpf, balance: 0})
    return `Conta do CPF ${cpf} criada com sucesso`;
}

exports.showBalance = (cpf) => {

    var balance = 0;
    var accountExists = false;

    bankAccounts.map((bankAccount, index) => {
        if ((bankAccount.cpf) == cpf) {
            balance = bankAccount.balance;
            accountExists = true
        }
    })

    if (accountExists) {
        return balance;
    } else {
        return 'Conta não existe'
    }
}
exports.withDraw = (cpf, value) => {

    var accountExists = false

    bankAccounts.map((bankAccount) => {
        if ((bankAccount.cpf) == cpf) {
            accountExists = true;
            bankAccount.balance = parseInt(bankAccount.balance) - parseInt(value);
        }
    })

    if (accountExists) {
        return 'Saque realizado'
    } else {
        return 'Conta não existe'
    }
}

exports.deposit = (cpf,value) => {

    var accountExists = false
    bankAccounts.map((bankAccount) => {
        if ((bankAccount.cpf) == cpf) {
            accountExists = true;
            bankAccount.balance = parseInt(bankAccount.balance) + parseInt(value);
        }
    })

    if (accountExists) {
        return 'Depósito realizado'
    } else {
        return 'Conta não existe'
    }
}

